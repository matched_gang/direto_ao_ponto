# módulo para fazer a integração com o Airtable
import requests
import psycopg2
import time
from more_itertools import chunked
from misse_en_place import imoveis_airtable

# Integração com o Airtable usando a API
api_key = 'keyZd2fC99S36mAf0'
base_id = 'apphNX5Z7c7hqqSGP'
table_name = 'tblQ3zwMEMdT8aUwM'

def post_imoveis_airtable(api_key, base_id, table_name, data):
    headers = {
        'Authorization': f'Bearer {api_key}',
        'Content-Type': 'application/json'
    }
    url = f'https://api.airtable.com/v0/{base_id}/{table_name}'
    response = requests.post(url, headers=headers, json=data)
    return response.json()

# Dividir a lista de dados em lotes de 10 registros cada
lotes = list(chunked(imoveis_airtable, 10))

# Aqui é que vamos limitar o número de lotes
lotes = lotes[:3]

# Contador para acompanhar o número de requisições
contador_requisicoes = 0

# Iterar sobre cada lote e enviar para o Airtable
for i, lote in enumerate(lotes, 1):
    # Enviar o lote para o Airtable
    response = post_imoveis_airtable(api_key, base_id, table_name, {'records': lote})

    # Imprimir a resposta do Airtable
    print(f"Resposta do lote {i}: {response}")

    # Verificar a resposta do Airtable
    if len(response['records']) == len(lote):
        # Se o número de registros na resposta é igual ao número de registros no lote, significa que todos foram inseridos com sucesso
        # Definir a flag de processamento como 22
        fl_processamento = 22
    else:
        # Caso contrário, ocorreu um erro ao inserir algum registro
        # Definir a flag de processamento como 25
        fl_processamento = 25

    # Estabelece a conexão com o banco de dados
    conn = psycopg2.connect(database="postgres", user="postgres", password="11060704", host="localhost", port="5432")
    cur = conn.cursor()

    for registro in lote:
        # Recuperar o endereço completo do registro atual
        logradouro = registro['fields']['street_name']
        numero = registro['fields']['street_number']

        # Definir os valores para a atualização
        values = (fl_processamento, logradouro, numero)

        # Executar a consulta SQL para atualizar a coluna flag_processamento
        query_update = "UPDATE sao_paulo_2016 SET fl_processamento = %s WHERE nm_logradouro = %s and nr_logradouro = %s;"
        # Executar a consulta com os valores dinâmicos
        cur.execute(query_update, values)
        conn.commit()

    cur.close()
    conn.close()

    # Incrementar o contador de requisições
    contador_requisicoes += 1

    # Verificar se atingiu o limite de requisições por segundo (5 requisições)
    if contador_requisicoes % 5 == 0:
        # Aguardar 1 segundo antes de continuar o envio de requisições
        time.sleep(1)

print("Salado Processamento Concluído.")
