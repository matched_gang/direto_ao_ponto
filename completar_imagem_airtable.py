from airtable import Airtable
from misse_en_place import imoveis_airtable, data_psql

# Configuração da conexão com o Airtable
base_key = 'apphNX5Z7c7hqqSGP'
table_name = 'tblQ3zwMEMdT8aUwM'
api_key = 'keyZd2fC99S36mAf0'
airtable = Airtable(base_key, table_name, api_key)

imagens_psql_airtable = []
for row in data_psql:
    logradouro = row[0]
    numero = row[1]
    url_imagem = row[9]
    record = {
        'fields': {
            'street_name': logradouro,
            'street_number': numero,
            'url_imagem': url_imagem
        }
    }
    imagens_psql_airtable.append(record)

# Obtém todas as linhas da tabela do Airtable
rows_airtable = airtable.get_all(formula="AND({street_name}, {street_number})")

# Itera sobre as linhas da tabela e adiciona a imagem correspondente
for row in rows_airtable:
    if 'street_name' in row['fields'] and 'street_number' in row['fields']:
        street_name = row['fields']['street_name']
        street_number = row['fields']['street_number']
        # Verifica se a linha já possui uma imagem associada
        if 'images' not in row['fields']:
            # Procura a URL da imagem correspondente na lista imagem_psql_airtable
            for data_row in imagens_psql_airtable:
                data_street_name = data_row['fields']['street_name']
                data_street_number = data_row['fields']['street_number']
                if street_name == data_street_name and street_number == data_street_number:
                    url_imagem = data_row['fields']['url_imagem']
                    # Atualiza a linha do Airtable com a URL da imagem
                    airtable.update(row['id'], {'images': [{'url': url_imagem}]})
                    break
            # Se não encontrou uma URL de imagem correspondente, usa a URL da imagem padrão
            if url_imagem is None:
                url_imagem = "https://images2.imgbox.com/9e/4a/zJkBdsyY_o.png"

            # Atualiza a linha do Airtable com a URL da imagem
            airtable.update(row['id'], {'images': [{'url': url_imagem}]})

print('---Upload de imagens concluído com sucesso!---')
