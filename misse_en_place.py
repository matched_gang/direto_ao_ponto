# -*- coding: utf-8 -*-
import requests
import psycopg2

#query SQL para obter os dados do banco e criar o dicionario airtable:
query = "SELECT COALESCE(nm_logradouro, ''), COALESCE(nr_logradouro, ''), COALESCE(nm_bairro, ''), 'São Paulo', 'SP', COALESCE(latitude), COALESCE(longitude), COALESCE(nr_areaterreno), COALESCE(vl_aluguel_total), COALESCE(img_url, ' ') FROM sao_paulo_2016 WHERE nm_bairro = 'CAMPO BELO' and nr_logradouro != '[NULL]' AND fl_processamento = '0' AND tp_usoimovel NOT IN ('APARTAMENTO EM CONDOMÍNIO', 'ARMAZÉNS GERAIS E DEPÓSITOS', 'ASILO', 'ORFANATO', 'CRECHE', 'SEMINÁRIO OU CONVENTO', 'CINEMA', 'CLUBE ESPORTIVO', 'CORTIÇO', 'ESCRITÓRIO/CONSULTÓRIO EM CONDOMÍNIO (UNIDADE AUTÔNOMA)', 'ESTAÇÃO RADIOEMISSORA', 'DE TELEVISÃO OU EMPRESA JORNALÍSTICA', 'FLAT DE USO COMERCIAL (SEMELHANTE A HOTEL)', 'FLAT RESIDENCIAL EM CONDOMÍNIO', 'GARAGEM (EXCLUSIVE EM PRÉDIO EM CONDOMÍNIO)', 'GARAGEM (UNIDADE AUTÔNOMA) DE PRÉDIO DE GARAGENS', 'GARAGEM (UNIDADE AUTÔNOMA) EM EDIFÍCIO EM CONDOMÍNIO DE ESCRITÓRIOS', 'CONSULTÓRIOS OU MISTO', 'GARAGEM (UNIDADE AUTÔNOMA) EM EDIFÍCIO EM CONDOMÍNIO DE USO EXCLUSIVAMENTE RESIDENCIAL', 'HOSPITAL', 'AMBULATÓRIO', 'CASA DE SAÚDE E ASSEMELHADOS', 'HOTEL', 'PENSÃO OU HOSPEDARIA', 'INDÚSTRIA', 'LOJA', 'OUTRAS EDIFICAÇÕES DE USO COLETIVO, COM UTILIZAÇÃO MÚLTIPLA', 'OUTRAS EDIFICAÇÕES DE USO COMERCIAL, COM UTILIZAÇÃO MÚLTIPLA', 'OUTRAS EDIFICAÇÕES DE USO DE SERVIÇO, COM UTILIZAÇÃO MÚLTIPLA', 'OUTRAS EDIFICAÇÕES DE USO ESPECIAL, COM UTILIZAÇÃO MÚLTIPLA', 'POSTO DE SERVIÇO', 'PRÉDIO DE APARTAMENTO, NÃO EM CONDOMÍNIO, DE USO EXCLUSIVAMENTE RESIDENCIAL', 'PRÉDIO DE APARTAMENTO, NÃO EM CONDOMÍNIO, DE USO MISTO (APARTAMENTOS E ESCRITÓRIOS E/OU CONSULTÓRIOS), COM OU SEM LOJA (PREDOMINÂNCIA RESIDENCIAL)', 'PRÉDIO DE ESCRITÓRIO OU CONSULTÓRIO, NÃO EM CONDOMÍNIO, COM OU SEM LOJAS', 'PRÉDIO DE ESCRITÓRIO, NÃO EM CONDOMÍNIO, DE USO MISTO (APARTAMENTOS E ESCRITÓRIOS E/OU CONSULTÓRIOS) COM OU SEM LOJA (PREDOMINÂNCIA COMERCIAL)', 'RESIDÊNCIA', 'RESIDÊNCIA COLETIVA', 'EXCLUSIVE CORTIÇO (MAIS DE UMA RESIDÊNCIA NO LOTE)', 'RESIDÊNCIA E OUTRO USO (PREDOMINÂNCIA RESIDENCIAL)')"

# Estabelece a conexão com o banco de dados
conn = psycopg2.connect(database="postgres", user="postgres", password="11060704", host="localhost", port="5432")
cur = conn.cursor()
cur.execute(query)
# Armazena todas as linhas retornadas pela consulta SQL em uma lista
rows = cur.fetchall()

# buscar dados do banco de dados psql
data_psql = rows

#desempacotar variaveis

imoveis_airtable = []

for row in data_psql:
    logradouro = row[0]
    numero = row[1]
    endereco_completo = numero + ', ' + logradouro
    bairro = row[2]
    latitude = row[5]
    longitude = row[6]
    area_privativa = row[7]
    preco = row[8]
    url_imagem = row[9]

    record = {
        'fields': {
            'status': 'ACTIVE',
            'notes': ' ', # a futuro integrar chatGPT trazer mais informações sobre as imediações do imóvel ('nm_logradouro' + 'nr_logradouro')
            'latitude': latitude,
            'longitude': longitude,
            'property_type': 'Loja / Salão / Ponto Comercial',
            'description': 'Boa localização. Perto de diversos empreendimentos comerciais e residenciais em construção.',
            'images': [],
            'private_area': area_privativa,
            # 'parking_lots': ' ',
            'price': preco,
            'city': 'São Paulo',
            'neighborhood': bairro,
            'street_name': logradouro,
            'street_number': numero,
            'contact_name': 'propietário',
            # 'contact_code': 'nuggo_prop',
            'contact_phone': '(11) _____-____'
        }
    }
    imoveis_airtable.append(record)
